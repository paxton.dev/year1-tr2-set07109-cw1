#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

int main(int argc, char** argv[])
{
    char const* const fileName = argv[1]; 
    FILE* file = fopen("input.txt", "r"); 
    char line[1000];
	
	char search_string[25];
	
	//Input string to be searched for
	printf("Please input a string to be searched for: ");
	scanf("%s", search_string);
	strupr(search_string);
  
	//Setting line number accumulator to 1
	int line_acc = 1;
	
	//Create the output file
	FILE *f = fopen("output.txt", "w");
	if (f == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}
	
	//Loops through each line of the text file
    while (fgets(line, sizeof(line), file)) 
	{

		//Setting all characters in line to uppercase.
	    strupr(line);
		
		//Check if the search_string exists in the current line and if so print it
		if (strstr(line, search_string)) 
		{
			//Writes text to the output file
			fprintf(f, "%s exists in line: %d regardless of case.\n", search_string, line_acc);
			fprintf(f, "full line: %s\n", line);
		}
		
		//Add one to the line accumulator so that the program knows which line each hello is on
		++line_acc;
    }

	fclose(f);
    fclose(file);
	
	printf("****PROGRAM COMPLETE****");

    return 0;
}
		
		//MAKEFILES FOR THE COMMANDS YES YES YES YES YES YES YES YES YES YES YES