all: clean find pulp_out_hey_no_caps pulp_out_hey_caps find_hey_stdin_stdout snatch_out_hey_no_caps snatch_out_hey_caps snatch_stdout_hey_caps

find:
	cl find.c
	
pulp_out_hey_no_caps:
	find hey -i pulp.txt -o pulp_out_hey_no_caps.txt
	
pulp_out_hey_caps:
	find hey -i pulp.txt -o pulp_out_hey_caps.txt -c
	
find_hey_stdin_stdout:
	find hey

snatch_out_hey_no_caps:
	find hey -i snatch.txt -o snatch_out_hey_no_caps.txt

snatch_out_hey_caps:
	find hey -i snatch.txt -o snatch_out_hey_caps.txt -c

snatch_stdout_hey_caps:
	find hey -i snatch.txt -c
	
clean:
	del *.obj
	del *.exe
	del *pulp_out_hey_no_caps.txt
	del *pulp_out_hey_caps.txt
	del *snatch_out_hey_no_caps.txt
	del *snatch_out_hey_caps.txt