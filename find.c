#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>

//Declare functions
int is_present(int argc, char **argv[]);
void read_file(int argc, char **argv[], FILE** file);
void out_file(int argc, char **argv[], FILE** outfile);

int main(int argc, char **argv[])
{
	//Variable declaration
	FILE* file;
	FILE* outfile;
	char line[1000];
	int line_acc = 1;
	int c_checker = 0;
	
	//Check if the program needs to be case sensitive
	c_checker = is_present(argc, argv);

	//Specify the file to be read.
	read_file(argc, argv, &file);

	//Create the output file
	out_file(argc, argv, &outfile);
	
	//Loops through each line of the text file
	while (fgets(line, sizeof(line), file)) 
	{
		//Check if program needs to be case sensitive
		if (c_checker == 1)
		{
			strupr(line);
			strupr(argv[1]);
		}
		
		//Check if the argv[1] exists in the current line and if so print it
		if (strstr(line, argv[1])) 
		{
			//Writes text to the output file
			fprintf(outfile, "%s exists in line: %d.\n", argv[1], line_acc);
			fprintf(outfile, "full line: %s\n", line);
		}

		//Add one to the line accumulator.
		++line_acc;
	}

	//Close files for memory management.
	fclose(outfile);
	fclose(file);
	printf("****PROGRAM COMPLETE****");
	return 0;
}

int is_present(int argc, char **argv[])
{
	for (int i=0;i<argc;++i)
		{
			if (strcmp(argv[i], "-c") == 0)
			{
				return 1;
			}
		}
}

void read_file(int argc, char **argv[], FILE** file)
{
	int i_checker = 0;
	for (int i=0;i<argc;++i)
	{
		if (strcmp(argv[i], "-i") == 0)
		{
			i_checker = 1;
			*file = fopen(argv[i+1], "r");
			if (*file == NULL)
			{
				printf("Sorry, There was an error opening your file!\n");
				exit(1);
			}
			else
			{
				printf("File opened successfully!\n");
			}
		}
	}
	if (i_checker == 0)
	{
		printf("You are now entering standard input, please use ctrl + z to end input.\n");
		*file = stdin; 
	}
}

void out_file(int argc, char **argv[], FILE** outfile)
{
	int o_checker = 0;
	for (int i=0;i<argc;++i)
	{
		if (strcmp(argv[i], "-o") == 0)
		{
			o_checker = 1;
			*outfile = fopen(argv[i+1], "w");
		}
	}
	if (o_checker == 0)
	{
		*outfile = stdout;
	}
	if (*outfile == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}
}