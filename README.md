The makefile (built in notepad++) provided has several commands, these include:

all - this cleans the folder, compiles the program, runs six example programs, two of which requires further user input to complete. the first of these being (pulp_out_hey_no_caps), the second being (pulp_out_hey_caps),  the third being (find_hey_stdin_stdout), the fourth being (snatch_out_hey_no_caps), the fifth being (snatch_out_hey_caps) and the sixth being (snatch_stdout_hey_caps)

find - compiles the main program.

pulp_out_hey_no_caps - this finds the string "hey" in a case sensitive manner inside a specified txt file and prints to a text file(simulates: find hey -i pulp.txt -o pulp_out_hey_no_caps.txt) 

pulp_out_hey_caps - this finds the string "hey" inside a specified txt file regardless of case and prints to a text file (simulates: find hey -i pulp.txt -o pulp_out_hey_caps.txt -c)

find_hey_stdin_stdout - This finds the string "hey" using stdin and printing to stdout (simulates: find hey)

snatch_out_hey_no_caps - This finds the string "hey" inside a second specified text file in a case sensitive manner and prints to a text file (simulates find hey -i snatch.txt -o snatch_out_hey_no_caps.txt)

snatch_out_hey_caps - This finds the string "hey" inside a second specified text file regardless of case and prints to a text file (simulates find hey -i snatch.txt -o snatch_out_hey_caps.txt -c)

snatch_stdout_hey_caps - This finds the string "hey" in a second specified text file regardless of case and prints to stdout (find hey -i snatch.txt -c)

clean - Deletes all files created by running the application